import { useEffect, useState } from 'react';
import { randomQuote } from 'randomquote-api';
import { hex } from '@vid3v/random-color';

function App() {
  const [data, setData] = useState({});
  const [color, setColor] = useState('');

  useEffect(() => {
    setData(GetQoute());
    setColor(hex())
  }, []);

  const GetQoute = () => {
    return randomQuote();
  }

  const handleClick = () => {
    setData(GetQoute());
    setColor(hex());
  }

  return (
    <div className='app' style={{ backgroundColor: color }}>
      <div id='quote-box' className='content'>
        <h3 id='text' className='quotes' style={{ color: color }}>" {data.quote} "</h3>
        <p id='author' className='author' style={{ color: color }}>--{data.author}</p>
        <div className='btn-container'>
          <div className='btn-left'>
            <a id='tweet-quote' href={"https://twitter.com/intent/tweet/hashtags=quotes&related=" + data.quote + data.author} target="_blank">
              <i className="fa-brands fa-twitter" style={{ fontSize: '2rem', color: color, marginRight: '1.5rem' }}></i>
            </a>
            <a href="https://gitlab.com/intrvrtcode/random-quotes-app/-/tree/main" target="_blank">
              <i className="fa-brands fa-gitlab" style={{ fontSize: '2rem', color: color }}></i>
            </a>
          </div>
          <div className='btn-right'>
            <button id='new-quote' style={{ backgroundColor: color }} onClick={handleClick}>New Quotes</button>
          </div>
        </div>
      </div>

      <footer>
      <a href="#">by intrvrtcode</a>
      <span>|</span>
      <a href="https://www.flaticon.com/free-icons/quote" title="quote icons">quote icons by Dave Gandy-Flaticon</a>
      </footer>
    </div>
  )
}

export default App
